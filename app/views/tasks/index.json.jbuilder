json.array!(@tasks) do |task|
  json.extract! task, :id, :task_name, :task_description, :task_complete, :job_id
  json.url task_url(task, format: :json)
end

json.array!(@employees) do |employee|
  json.extract! employee, :id, :fname, :lname, :job_id
  json.url employee_url(employee, format: :json)
end

json.array!(@jobs) do |job|
  json.extract! job, :id, :job_name, :due_date, :job_complete, :job_location
  json.url job_url(job, format: :json)
end

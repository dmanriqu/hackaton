class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :job_name
      t.date :due_date
      t.boolean :job_complete, :default => false
      t.string :job_location

      t.timestamps
    end
  end
end

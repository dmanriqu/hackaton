class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :task_name
      t.string :task_description
      t.boolean :task_complete, :default => false
      t.integer :job_id

      t.timestamps
    end
  end
end
